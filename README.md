# sisop-praktikum-modul-4-2023-BS-D11

# Kelompok D11 - SISOP D 
## 5025211196 - Sandyatama Fransisna Nugraha
## 5025211105 - Sarah Nurhasna Khairunnisa
## 5025211148 - Katarina Inezita Prambudi

<br>

---
# Soal 1
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 

Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.

Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

 a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.
    
    kaggle datasets download -d bryanb/fifa-player-stats-database
    
Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

<br>

b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

<br>

c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.

Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

<br>

d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?

Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

<br>

e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

<br>

---
## Penyelesaian

### 1. Untuk menyelesaikan 1a dan 1b, buatlah file storage.c dengan code sebagai berikut:
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX 10000

// Fungsi download file dari kaggle
void download(){
    // cek apakah file sudah pernah di download atau belum
    FILE* file = fopen("fifa-player-stats-database.zip", "r");
    if (file != NULL) {
        printf("File already exists.\n");
        fclose(file);
        return;
    }
    // download file
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    printf("Download finished\n");

}

// Fungsi unzip file fifa-player-stats-database.zip yang telah di download
void unzip(){
    // cek apakah file sudah pernah di extract atau belum
    FILE* file = fopen("FIFA23_official_data.csv", "r");
    if (file != NULL) {
        printf("File already extracted.\n");
        fclose(file);
        return;
    }
    // extract file
    system("unzip fifa-player-stats-database.zip");
    printf("Extract finished\n");
}

// Fungsi analisis data unutk mencetak data yang diinginkan
void analyzeData() {
    // Open file FIFA23_official_data.csv
    FILE* file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char line[MAX];

    // Baca baris pertama (header)
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("Error reading file.\n");
        fclose(file);
        return;
    }

    // Cetak header kolom yang diinginkan
    printf("\tName\t\t\tAge\t\t\tClub\t\t\tNationality\t\tPotential\t\t\t\tPhoto\n");

    // Baca dan proses baris-baris selanjutnya
    while (fgets(line, sizeof(line), file)) {
        char* token = strtok(line, ",");
        char* name;
        char* age;
        char* club;
        char* nationality;
        char* potential;
        char* photo;

        // Cari indeks kolom yang diperlukan
        int index = 0;
        while (token != NULL) {
            if (index == 1) {
                name = token;
            } else if (index == 2) {
                age = token;
            } else if (index == 8) {
                club = token;
            } else if (index == 4) {
                nationality = token;
            } else if (index == 7) {
                potential = token;
            } else if (index == 3) {
                photo = token;
            }

            token = strtok(NULL, ",");
            index++;
        }

        // Cek kriteria umur dan klub
        if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
            // Cetak nilai yang diinginkan
            printf("%20s\t%10s\t\t%10s\t%25s\t%15s\t\t%60s\n", name, age, club, nationality, potential, photo);
        }
    }

    // Close file FIFA23_official_data.csv
    fclose(file);
}

int main(){
    pid_t pid = fork();
    int status;
    // Parent process
    if (pid < 0) {
        perror("Error: Fork failed");
        exit(1);
    } 
    // Child process
    else if (pid == 0){
        // Download file
        download();
        // Extract file
        unzip();
    }
    wait(&status);

    printf("\nBerikut ini merupakan data pemain FIFA23:\n");
    // Baca dan analisis file
    analyzeData();
    return 0;
}
```
### Berikut merupakan penjelasan code pada file storage.c :
1) Untuk mendownload file dari kaggle, buat fungsi download() yaitu:
    ```c
    void download(){
        // cek apakah file sudah pernah di download atau belum
        FILE* file = fopen("fifa-player-stats-database.zip", "r");
        if (file != NULL) {
            printf("File already exists.\n");
            fclose(file);
            return;
        }
        // download file
        system("kaggle datasets download -d bryanb/fifa-player-stats-database");
        printf("Download finished\n");

    }
    ```  
    - Agar tidak ada duplikasi file ketika mendownload, cek terlebih dahulu apakah file yang akan di download sudah tersedia atau belum, yaitu dengan:
        ```c 
        FILE* file = fopen("fifa-player-stats-database.zip", "r");
        if (file != NULL) {
            printf("File already exists.\n");
            fclose(file);
            return;
        }
        ```   
    - Gunakan system untuk menjalankan command `kaggle datasets download -d bryanb/fifa-player-stats-database`.

<br>

2) Untuk melakukan unzip file yang sudah didownload, buat fungsi unzip() yaitu sebagai berikut:
    ```c
    void unzip(){
        // cek apakah file sudah pernah di extract atau belum
        FILE* file = fopen("FIFA23_official_data.csv", "r");
        if (file != NULL) {
            printf("File already extracted.\n");
            fclose(file);
            return;
        }
        // extract file
        system("unzip fifa-player-stats-database.zip");
        printf("Extract finished\n");
    }
    ```
    - Agar tidak ada duplikasi file, cek terlebih dahulu apakah file yang telah diunzip sudah tersedia atau belum, yaitu dengan:
        ```c 
        FILE* file = fopen("FIFA23_official_data.csv", "r");
        if (file != NULL) {
            printf("File already extracted.\n");
            fclose(file);
            return;
        }
        ```   
    - Gunakan system untuk menjalankan command `unzip fifa-player-stats-database.zip`.

<br>

3) Untuk melakukan analisis data yang diinginkan, buat fungsi analyzeData() sebagai berikut:
    ```c
    void analyzeData() {
        // Open file FIFA23_official_data.csv
        FILE* file = fopen("FIFA23_official_data.csv", "r");
        if (file == NULL) {
            printf("Error opening file.\n");
            return;
        }

        char line[MAX];

        // Baca baris pertama (header)
        if (fgets(line, sizeof(line), file) == NULL) {
            printf("Error reading file.\n");
            fclose(file);
            return;
        }

        // Cetak header kolom yang diinginkan
        printf("\tName\t\t\tAge\t\t\tClub\t\t\tNationality\t\tPotential\t\t\t\tPhoto\n");

        // Baca dan proses baris-baris selanjutnya
        while (fgets(line, sizeof(line), file)) {
            char* token = strtok(line, ",");
            char* name;
            char* age;
            char* club;
            char* nationality;
            char* potential;
            char* photo;

            // Cari indeks kolom yang diperlukan
            int index = 0;
            while (token != NULL) {
                if (index == 1) {
                    name = token;
                } else if (index == 2) {
                    age = token;
                } else if (index == 8) {
                    club = token;
                } else if (index == 4) {
                    nationality = token;
                } else if (index == 7) {
                    potential = token;
                } else if (index == 3) {
                    photo = token;
                }

                token = strtok(NULL, ",");
                index++;
            }

            // Cek kriteria umur dan klub
            if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
                // Cetak nilai yang diinginkan
                printf("%20s\t%10s\t\t%10s\t%25s\t%15s\t\t%60s\n", name, age, club, nationality, potential, photo);
            }
        }

        // Close file FIFA23_official_data.csv
        fclose(file);
    }
    ```
    - Data dalam file FIFA23_official_data.csv akan disimpan ke dalam array `line`. 
    - Pada bagian berikut
        ```c
        if (fgets(line, sizeof(line), file) == NULL) {
            printf("Error reading file.\n");
            fclose(file);
            return;
        }
        ```
        Fungsi `fgets(line, sizeof(line), file)` digunakan untuk membaca baris pertama di dalam file dan kemudian menyimpannya ke dalam array `line`.

    - Di dalam while loop, `fgets(line, sizeof(line), file)` digunakan untuk membaca baris-baris berikutnya hingga tidak ada lagi baris yang dapat dibaca.
    - Pada bagian `char* token = strtok(line, ",");`, fungsi strtok() digunakan untuk memecah baris pada `line` yang dipisahkan oleh delimiter `","`
    - Untuk mengambil data-data yang diinginkan, yaitu nama, umur, asal club, asal negara, potensi, dan foto, maka akan diinisialisasi dalam while loop dengan mengambil nilai token yang sesuai berdasarkan indeks kolom. Selama token masih ada, loop akan berlanjut untuk mendapatkan nilai token yang sesuai untuk setiap kolom dalam baris yang sama menggunakan `strtok(NULL, ",");` dan menambahkan nilai indeks.
    - Selanjutnya untuk mengambil data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City, dilakukan pengecekan sebagai berikut:
        ```c
        if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
            // Cetak nilai yang diinginkan
            printf("%20s\t%10s\t\t%10s\t%25s\t%15s\t\t%60s\n", name, age, club, nationality, potential, photo);
        }
        ```
        - Karena `age` dan `potential` merupakan string, maka perlu dilakukan convert nilai string ke integer menggunakan fungsi atoi() agar dapat melakukan pengecekan.
        - Gunakan fungsi strcmp() untuk membandingkan value `club` dengan string "Manchester City".

<br>

4) Fungsi main:
    ```c
    int main(){
        pid_t pid = fork();
        int status;
        // Parent process
        if (pid < 0) {
            perror("Error: Fork failed");
            exit(1);
        } 
        // Child process
        else if (pid == 0){
            // Download file
            download();
            // Extract file
            unzip();
        }
        wait(&status);

        printf("\nBerikut ini merupakan data pemain FIFA23:\n");
        // Baca dan analisis file
        analyzeData();
        return 0;
    }
    ```
    - Pada fungsi main, child process akan menjalankan fungsi `donwload()` dan `unzip()`, sementara parent process akan menunggu child process menyelesaikan tugasnya lalu parent process akan menjalankan fungsi `analyzeData()`.

<br>

### 2. Buat dockerfile dengan code sebagai berikut untuk menyelesaikan 1c :
```c
FROM python:3.7

# Mengatur direktori kerja di dalam container
WORKDIR /app

# Set up kaggle
# Source: https://github.com/Kaggle/kaggle-api 
RUN apt-get update
RUN apt-get install -y python3-pip
RUN pip install kaggle
COPY kaggle.json /root/.kaggle/kaggle.json

EXPOSE 8080

# Copy file storage.c
COPY storage.c .

# Compile storage.c
RUN gcc -o storage storage.c

# Run storage.c
CMD ["./storage"]
```
- Untuk set up kaggle, perlu install `python3-pip` agar dapat menggunakan command `pip` untuk meng-install kaggle.
- Copy file `kaggle.json` dari direktori lokal ke direktori `/root/.kaggle/` di dalam container. File `kaggle.json` ini berisi kunci API Kaggle yang diperlukan untuk mengakses dan mengunduh dataset.
- Copy file storage.c ke dalam container:
    ```
    COPY storage.c .
    ```
- Compile file storage.c di dalam container
    ```
    RUN gcc -o storage storage.c
    ```
- Run file storage.c yang telah dicompile di dalam container
    ```
    CMD ["./storage"]
    ```

<br>

### 3. Build dockerfile dan buat docker image menggunakan command berikut:
```
sudo docker build -t storage-app .
```
- `storage-app` merupakan nama imagenya.
- Tanda titik (.) di akhir perintah menunjukkan bahwa dockerfile berada di direktori saat ini.

<br>

### 4. Run image agar dapat mem-verifikasi output docker image dengan menggunakan command berikut:
```
sudo docker run storage-app
```
- output image yang diinginkan yaitu output yang sama ketika menjalankan file storage

<br>

### 5. Lakukan perintah berikut untuk mem-publish docker image ke docker hub untuk menyelesaikan 1d
- Login docker:
    ```
    docker login
    ```
- Push image ke docker hub:
    ```
    docker push {Username}/storage-app
    ```
    Dalam hal ini, perintah yang saya gunakan adalah sebagai berikut:
    ```
    docker push sarahnrhsna/storage-app
    ```
- Docker image akan bisa diakses secara public melalui:
    ```
    https://hub.docker.com/r/{Username}/storage-app 
    ```
    Dalam hal ini, link docker hub saya adalah sebagai berikut :
    ```
    https://hub.docker.com/r/sarahnrhsna/storage-app 
    ```
    User lain dapat mengunduh dan menggunakan image tersebut.

<br>

### 6. Buat file docker compose yang berisi code berikut dan buat 2 direktori terpisah bernama `Barcelona` dan `Napoli` untuk menyelesaikan 1e :
#### docker-compose.yml :
```c
version: '3'
services:
  barcelona:
    build: ./Barcelona
    image: sarahnrhsna/storage-app
    ports:
      - 8081-8085:8080
    deploy:
      replicas: 5
  napoli:
    build: ./Napoli
    image: sarahnrhsna/storage-app 
    ports:
      - 8086-8090:8080
    deploy:
      replicas: 5
```
- Docker compose berisi 2 service yaitu `barcelona` dan `napoli`
- Service barcelona akan menggunakan direktori `./Barcelona` untuk membangun image `sarahnrhsna/storage-app`. Image akan direplikasi sebanyak 5 kali untuk membuat instance sebanyak 5, dan akan menggunakan 5 alokasi port yaitu `8081-8085` di host ke port `8080` di kontainer docker.
- Service napoli akan menggunakan direktori `./Barcelona` untuk membangun image `sarahnrhsna/storage-app`. Image akan direplikasi sebanyak 5 kali untuk membuat instance sebanyak 5, dan akan menggunakan 5 alokasi port yaitu `8086-8090` di host ke port `8080` di kontainer docker.

<br>

---
## Dokumentasi Output
1. Run storage.c
<br>
![cronjobs](img/run_storage.c.png)
<br>
2. Build docker
<br>
![builddocker](img/builddocker.png)
<br>
3. Run docker
<br>
![rundocker](img/run_docker.png)
<br>
4. Docker hub
<br>
![dockerhub](img/dockerhub.png)
<br>

---
## Kendala
- Terdapat kendala di hari Sabtu, 3 Juni, VMware yang saya gunakan error dan tidak bisa login ke ubuntunya(black screen sebelum login screen) sehingga untuk output docker-composenya tidak sempat untuk di screenshot.
---
<br>

---
# Soal 3
a. Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c

<br>

b. Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat). 

<br>

c. Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

<br>

d. Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

<br>

e. Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.

Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 

Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun.

<br>

---
## Penyelesaian

---
## Dokumentasi Output + Kendala

1. Ketika melakukan instalasi, muncul kendala seperti gambar dibawah
<br>
![cronjobs](img/kendala1.png)
<br>
2. Mencoba dengan `ps aux | grep -i apt `. Didapatkan hasil seperti gambar dibawah
<br>
![builddocker](img/kendala2.png)
<br>
3. Melakukan ` sudo kill -9 1198 ` dan mencoba menjalankan ` sudo apt-get install \ ca-certificates \ curl \ gnupg `. Didapatkan hasil seperti dibawah ini.'
<br>
![rundocker](img/kendala3.png)
<br>
4. Melakukan ` sudo dpkg --configure -a  `. Didapatkan hasil seperti dibawah ini
<br>
![dockerhub](img/kendala4.png)
<br>
5. Melakukan ` sudo apt-get update `. Didapatkan hasil seperti dibawah ini
<br>
![cronjobs](img/kendala5.png)
<br>
6. Melakukan ` sudo apt-get install \ ca-certificates \ curl \ gnupg `. Didapatkan hasil seperti gambar dibawah
<br>
![builddocker](img/kendala6.png)
<br>
7. MMelakukan instalasi docker seperti pada modul 4.Didapatkan hasil seperti dibawah ini.'
<br>
![rundocker](img/kendala7.png)
<br>
8. Melakukan update. Didapatkan hasil seperti dibawah ini
<br>
![dockerhub](img/kendala8.png)
<br>
9. Docker tidak berhasil dijalankan. Didapatkan hasil seperti dibawah ini
<br>
![dockerhub](img/kendala9.png)
<br>
![dockerhub](img/kendala10.png)

<br>

---
# Soal 4
a. Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 

<br>

b. Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.

<br>
c. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

<br>

d. Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.

Contoh:

file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

<br>

e. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

<br>

---
## Penyelesaian

### 1. Untuk menyelesaikan 1a dan 1b, buatlah file storage.c dengan code sebagai berikut:
```c
#define FUSE_USE_VERSION 28
#include <stdlib.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/tama/Documents";

static int is_module_dir(const char *path) {
    // Check if the directory name starts with "module_"
    char *dir_name = strdup(basename(strdup(path)));
    if (strstr(dir_name, "module_") == dir_name) {
        free(dir_name);
        return 1;
    }
    free(dir_name);
    return 0;
}

static void log_system_call(const char *level, const char *command, const char *desc1, const char *desc2) {
    char log_file_path[PATH_MAX];
    strcpy(log_file_path, "/home/tama/fs_module.log");
    FILE *log_file = fopen(log_file_path, "a");

    if (log_file != NULL) {
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        fprintf(log_file, "[%s]::%02d%02d%02d-%02d:%02d:%02d::%s::%s", level,
                tm.tm_year % 100, tm.tm_mon + 1, tm.tm_mday,
                tm.tm_hour, tm.tm_min, tm.tm_sec,
                command, desc1);

        if (desc2 != NULL) {
            fprintf(log_file, "::%s", desc2);
        }

        fprintf(log_file, "\n");
        fclose(log_file);
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);
    
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        //if (filler(buf, de->d_name, &st, 0))
          //  break;
        
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }
    closedir(dp);

    // Log the system call
    log_system_call("REPORT", "READDIR", path, NULL);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    res = rename(from, to);
    if (res == -1)
        return -errno;

    // Check if the renamed directory is a module
    if (is_module_dir(from) && !is_module_dir(to)) {
        // Restore the original content by combining the small files into one
        DIR *dp;
        struct dirent *de;
        char small_file[PATH_MAX];

        dp = opendir(to);
        if (dp != NULL) {
            FILE *out_file = fopen(to, "wb");
            if (out_file != NULL) {
                while ((de = readdir(dp)) != NULL) {
                    if (strncmp(de->d_name, ".", 1) != 0 && strncmp(de->d_name, "..", 2) != 0) {
                        sprintf(small_file, "%s/%s", to, de->d_name);
                        FILE *in_file = fopen(small_file, "rb");
                        if (in_file != NULL) {
                            char buffer[1024];
                            size_t bytesRead;

                            while ((bytesRead = fread(buffer, 1, sizeof(buffer), in_file)) > 0) {
                                fwrite(buffer, 1, bytesRead, out_file);
                            }

                            fclose(in_file);
                            remove(small_file);
                        }
                    }
                }

                fclose(out_file);
            }

            closedir(dp);
        }

        // Rename the contents of subdirectories recursively
        dp = opendir(to);
        if (dp != NULL) {
            while ((de = readdir(dp)) != NULL) {
                if (de->d_type == DT_DIR) {
                    if (strncmp(de->d_name, ".", 1) != 0 && strncmp(de->d_name, "..", 2) != 0) {
                        char sub_from[PATH_MAX], sub_to[PATH_MAX];
                        sprintf(sub_from, "%s/%s", to, de->d_name);
                        sprintf(sub_to, "%s/%s", from, de->d_name);
                        xmp_rename(sub_from, sub_to);
                    }
                }
            }

            closedir(dp);
        }
    }

    // Log the system call
    if (is_module_dir(to)) {
        log_system_call("FLAG", "RENAME", from, to);
    } else {
        log_system_call("REPORT", "RENAME", from, to);
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    // Check if the created directory is a module
    if (is_module_dir(path)) {
        // Append " (modular)" to the directory name
        size_t path_len = strlen(path);
        char new_path[path_len + 12];
        strcpy(new_path, path);
        strcat(new_path, " (modular)");
        res = rename(path, new_path);
        if (res == -1)
            return -errno;

        // Log the system call
        log_system_call("FLAG", "MKDIR", path, NULL);
    } else {
        // Log the system call
        log_system_call("REPORT", "MKDIR", path, NULL);
    }

    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    int res;
    res = creat(path, mode);
    if (res == -1)
        return -errno;

    close(res);

    // Check if the created file is in a module directory
    char *dir_path = strdup(path);
    char *dir_name = dirname(dir_path);
    if (is_module_dir(dir_name)) {
        // Split the file into small files
        FILE *in_file = fopen(path, "rb");
        if (in_file != NULL) {
            char small_file[PATH_MAX];
            size_t chunkSize = 1024;
            size_t bytesRead;
            int count = 0;

            while ((bytesRead = fread(small_file, 1, chunkSize, in_file)) > 0) {
                sprintf(small_file, "%s.%03d", path, count++);
                FILE *out_file = fopen(small_file, "wb");
                if (out_file != NULL) {
                    fwrite(small_file, 1, bytesRead, out_file);
                    fclose(out_file);
                }
            }

            fclose(in_file);
            remove(path);
        }
    }

    free(dir_path);

    // Log the system call
    if (is_module_dir(dirname("/home/tama/fs_module.log"))) {
        log_system_call("FLAG", "CREATE", path, NULL);
    } else {
        log_system_call("REPORT", "CREATE", path, NULL);
    }

    return 0;
}

static int xmp_rmdir(const char *path)
{
    int res;
    res = rmdir(path);
    if (res == -1)
        return -errno;

    // Log the system call
    log_system_call("FLAG", "RMDIR", path, NULL);

    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    res = unlink(path);
    if (res == -1)
        return -errno;

    // Log the system call
    log_system_call("FLAG", "UNLINK", path, NULL);

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .mkdir = xmp_mkdir,
    .create = xmp_create,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}


```
### Berikut merupakan penjelasan code pada file modular.c :
1) is_module_dir(const char *path):
    - Fungsi ini digunakan untuk mengecek apakah sebuah direktori memiliki awalan "module_". Fungsi ini berguna untuk mengidentifikasi apakah sebuah direktori termasuk dalam kategori modul atau bukan. 
    - Jika sebuah direktori memiliki awalan "module_", maka dianggap sebagai direktori yang berisi modul. Fungsi ini mengembalikan nilai 1 jika direktori memiliki awalan "module_", dan 0 jika tidak.

    <br>

    ```c
    static int is_module_dir(const char *path) {
    // Check if the directory name starts with "module_"
    char *dir_name = strdup(basename(strdup(path)));
    if (strstr(dir_name, "module_") == dir_name) {
        free(dir_name);
        return 1;
    }
    
    free(dir_name);
    return 0;

    }

    ```  
   
<br>

2) log_system_call(const char *level, const char *command, const char *desc1, const char *desc2):
    - Fungsi ini digunakan untuk mencatat setiap pemanggilan sistem ke dalam sebuah file log. Fungsi ini berguna untuk memantau aktivitas yang terjadi pada sistem file. Parameter level menunjukkan level log (REPORT atau FLAG). Jika level adalah REPORT, maka pemanggilan sistem akan dicatat dengan level biasa. 
    - Jika level adalah FLAG, maka pemanggilan sistem akan dicatat dengan tanda bendera yang menunjukkan adanya aktivitas yang mencurigakan. Parameter command menunjukkan sistem yang dipanggil (READDIR, RENAME, MKDIR, CREATE, RMDIR, atau UNLINK). 
    - Parameter desc1 dan desc2 adalah deskripsi terkait dengan pemanggilan sistem. Deskripsi ini dapat digunakan untuk memberikan informasi lebih detail tentang pemanggilan sistem yang dicatat.

    <br>

    ```c
    static void log_system_call(const char *level, const char *command, const char *desc1, const char *desc2) {
    char log_file_path[PATH_MAX];
    strcpy(log_file_path, "/home/tama/fs_module.log");
    FILE *log_file = fopen(log_file_path, "a");

    if (log_file != NULL) {
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        fprintf(log_file, "[%s]::%02d%02d%02d-%02d:%02d:%02d::%s::%s", level,
                tm.tm_year % 100, tm.tm_mon + 1, tm.tm_mday,
                tm.tm_hour, tm.tm_min, tm.tm_sec,
                command, desc1);

        if (desc2 != NULL) {
            fprintf(log_file, "::%s", desc2);
        }

        fprintf(log_file, "\n");
        fclose(log_file);
    }
}
    ```

<br>

3) xmp_getattr(const char *path, struct stat *stbuf):
    - Fungsi ini digunakan untuk mendapatkan atribut dari suatu file atau direktori. Fungsi ini akan mengisi struktur stbuf dengan informasi atribut dari file atau direktori yang ditunjuk oleh path. 
    - Informasi atribut yang didapatkan antara lain adalah ukuran file, waktu modifikasi, hak akses, dan tipe file. 

    <br>

    ```c
    static int xmp_getattr(const char *path, struct stat *stbuf){
        int res;
        char fpath[1000];

        sprintf(fpath,"%s%s",dirpath,path);

        res = lstat(fpath, stbuf);

        if (res == -1) return -errno;

        return 0;
    }

    ```  
   
<br>

4) xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi):
    - Fungsi ini digunakan untuk membaca isi direktori pada suatu path. Fungsi ini akan mengisi buffer buf dengan daftar file dan direktori yang terdapat pada direktori yang ditunjuk oleh path. 
    - Daftar file dan direktori yang dihasilkan akan diisi ke dalam buffer menggunakan callback function filler. Fungsi filler akan dipanggil beberapa kali untuk mengisi buffer dengan daftar file dan direktori yang terdapat pada direktori yang ditunjuk oleh path.

    <br>

    ```c
    static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    
    char fpath[1000];

    if(strcmp(path,"/") == 0){
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
        
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);
        
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        //if (filler(buf, de->d_name, &st, 0))
        //  break;
            
        res = (filler(buf, de->d_name, &st, 0));
            if(res!=0) break;
        }

        closedir(dp);

        // Log the system call
        log_system_call("REPORT", "READDIR", path, NULL);

        return 0;
    }


    ```  
   
<br>

5) xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi):
    -  Fungsi ini digunakan untuk membaca isi dari sebuah file. Fungsi ini akan mengisi buffer buf dengan isi dari file yang ditunjuk oleh path. Isi dari file akan dibaca dari offset tertentu dengan ukuran tertentu yang ditentukan oleh parameter offset dan size. 

    <br>

    ```c
   static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;}

    ```  
   
<br>

6) xmp_rename(const char *from, const char *to):
    - Fungsi ini digunakan untuk mengubah nama sebuah file atau direktori. Fungsi ini akan mengubah nama file atau direktori yang ditunjuk oleh from menjadi nama yang ditunjuk oleh to. 

    <br>

    ```c
    static int xmp_rename(const char *from, const char *to){
    int res;
    res = rename(from, to);
    if (res == -1)
        return -errno;

    // Check if the renamed directory is a module
    if (is_module_dir(from) && !is_module_dir(to)) {
        // Restore the original content by combining the small files into one
        DIR *dp;
        struct dirent *de;
        char small_file[PATH_MAX];

        dp = opendir(to);
        if (dp != NULL) {
            FILE *out_file = fopen(to, "wb");
            if (out_file != NULL) {
                while ((de = readdir(dp)) != NULL) {
                    if (strncmp(de->d_name, ".", 1) != 0 && strncmp(de->d_name, "..", 2) != 0) {
                        sprintf(small_file, "%s/%s", to, de->d_name);
                        FILE *in_file = fopen(small_file, "rb");
                        if (in_file != NULL) {
                            char buffer[1024];
                            size_t bytesRead;

                            while ((bytesRead = fread(buffer, 1, sizeof(buffer), in_file)) > 0) {
                                fwrite(buffer, 1, bytesRead, out_file);
                            }

                            fclose(in_file);
                            remove(small_file);
                        }
                    }
                }

                fclose(out_file);
            }

            closedir(dp);
        }

        // Rename the contents of subdirectories recursively
        dp = opendir(to);
        if (dp != NULL) {
            while ((de = readdir(dp)) != NULL) {
                if (de->d_type == DT_DIR) {
                    if (strncmp(de->d_name, ".", 1) != 0 && strncmp(de->d_name, "..", 2) != 0) {
                        char sub_from[PATH_MAX], sub_to[PATH_MAX];
                        sprintf(sub_from, "%s/%s", to, de->d_name);
                        sprintf(sub_to, "%s/%s", from, de->d_name);
                        xmp_rename(sub_from, sub_to);
                    }
                }
            }

            closedir(dp);
        }
    }

    // Log the system call
    if (is_module_dir(to)) {
        log_system_call("FLAG", "RENAME", from, to);
    } else {
        log_system_call("REPORT", "RENAME", from, to);
    }

    return 0;
    }


    ```  
   
<br>

7) xmp_mkdir(const char *path, mode_t mode): 
    - Fungsi ini digunakan untuk membuat sebuah direktori. Fungsi ini akan membuat sebuah direktori pada path yang ditunjuk oleh path. Direktori akan dibuat dengan hak akses tertentu yang ditentukan oleh parameter mode. 
    

    <br>

    ```c
    static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    // Check if the created directory is a module
    if (is_module_dir(path)) {
        // Append " (modular)" to the directory name
        size_t path_len = strlen(path);
        char new_path[path_len + 12];
        strcpy(new_path, path);
        strcat(new_path, " (modular)");
        res = rename(path, new_path);
        if (res == -1)
            return -errno;

        // Log the system call
        log_system_call("FLAG", "MKDIR", path, NULL);
    } else {
        // Log the system call
        log_system_call("REPORT", "MKDIR", path, NULL);
    }

    return 0;
    }

    ```  
   
<br>

8) xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi):
    - Fungsi ini digunakan untuk membuat sebuah file baru. Fungsi ini akan membuat sebuah file pada path yang ditunjuk oleh path. File akan dibuat dengan hak akses tertentu yang ditentukan oleh parameter mode. 

    <br>

    ```c
    static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi){
    int res;
    res = creat(path, mode);
    if (res == -1)
        return -errno;

    close(res);

    // Check if the created file is in a module directory
    char *dir_path = strdup(path);
    char *dir_name = dirname(dir_path);
    if (is_module_dir(dir_name)) {
        // Split the file into small files
        FILE *in_file = fopen(path, "rb");
        if (in_file != NULL) {
            char small_file[PATH_MAX];
            size_t chunkSize = 1024;
            size_t bytesRead;
            int count = 0;

            while ((bytesRead = fread(small_file, 1, chunkSize, in_file)) > 0) {
                sprintf(small_file, "%s.%03d", path, count++);
                FILE *out_file = fopen(small_file, "wb");
                if (out_file != NULL) {
                    fwrite(small_file, 1, bytesRead, out_file);
                    fclose(out_file);
                }
            }

            fclose(in_file);
            remove(path);
        }
    }

    free(dir_path);

    // Log the system call
    if (is_module_dir(dirname("/home/tama/fs_module.log"))) {
        log_system_call("FLAG", "CREATE", path, NULL);
    } else {
        log_system_call("REPORT", "CREATE", path, NULL);
    }

    return 0;
    }

    ```  
   
<br>

9) xmp_rmdir(const char *path):
    - Fungsi ini digunakan untuk menghapus sebuah direktori. Fungsi ini akan menghapus sebuah direktori pada path yang ditunjuk oleh path. 

    <br>

    ```c
    static int xmp_rmdir(const char *path){
    int res;
    res = rmdir(path);
    if (res == -1)
        return -errno;

    // Log the system call
    log_system_call("FLAG", "RMDIR", path, NULL);

    return 0;
    }

    ```  
   
<br>

10) xmp_unlink(const char *path):
    - Fungsi ini digunakan untuk menghapus sebuah file. Fungsi ini akan menghapus sebuah file pada path yang ditunjuk oleh path. 

    <br>

    ```c
    static int xmp_unlink(const char *path){
    int res;
    res = unlink(path);
    if (res == -1)
        return -errno;

    // Log the system call
    log_system_call("FLAG", "UNLINK", path, NULL);

    return 0;
    }

    ```  
   
<br>

11) xmp_oper:
    - Struktur yang berisi setiap fungsi yang akan dijalankan oleh FUSE. Struktur ini akan digunakan oleh FUSE untuk memanggil fungsi-fungsi yang telah didefinisikan oleh pengguna. 

    <br>

    ```c
    static struct fuse_operations xmp_oper = {
        .getattr = xmp_getattr,
        .readdir = xmp_readdir,
        .read = xmp_read,
        .rename = xmp_rename,
        .mkdir = xmp_mkdir,
        .create = xmp_create,
        .rmdir = xmp_rmdir,
        .unlink = xmp_unlink,
    };

    ```  
   
<br>

4) Fungsi main:
    ```c
    int main(int argc, char *argv[]){
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
    }

    ```
    - Fungsi utama program yang melakukan inisialisasi dan menjalankan FUSE. Fungsi ini akan menginisialisasi FUSE dan mengatur setiap fungsi yang akan dijalankan oleh FUSE. Setelah itu, fungsi ini akan menjalankan FUSE hingga program dihentikan.

<br>

---
## Dokumentasi Output
1. Hasil dari program menghasilkan mount
<br>
![cronjobs](img/hasilmount.png)
<br>
2. Hasil log dari program 
<br>
![builddocker](img/hasillog.png)
<br>
3. Isi dalam mount
<br>
![rundocker](img/dalammount.png)
<br>


---
## Kendala
- Saya kurang memahami soal yang mengatakan harus menjadikan direktori baru menjadi direktori modular yang dimana membagi ukuran menjadi lebih kecil
- Modul fuse sudah memahami secara teori, namun ketika dihadapkan dengan praktek. Cukup kesulitan dengan modul ini dibandingkan modul sebelumnya
---
<br>


