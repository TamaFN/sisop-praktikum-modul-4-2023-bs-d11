#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX 10000

// Fungsi download file dari kaggle
void download(){
    // cek apakah file sudah pernah di download atau belum
    FILE* file = fopen("fifa-player-stats-database.zip", "r");
    if (file != NULL) {
        printf("File already exists.\n");
        fclose(file);
        return;
    }
    // download file
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    printf("Download finished\n");

}

// Fungsi unzip file fifa-player-stats-database.zip yang telah di download
void unzip(){
    // cek apakah file sudah pernah di extract atau belum
    FILE* file = fopen("FIFA23_official_data.csv", "r");
    if (file != NULL) {
        printf("File already extracted.\n");
        fclose(file);
        return;
    }
    // extract file
    system("unzip fifa-player-stats-database.zip");
    printf("Extract finished\n");
}

// Fungsi analisis data unutk mencetak data yang diinginkan
void analyzeData() {
    // Open file FIFA23_official_data.csv
    FILE* file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char line[MAX];

    // Baca baris pertama (header)
    if (fgets(line, sizeof(line), file) == NULL) {
        printf("Error reading file.\n");
        fclose(file);
        return;
    }

    // Cetak header kolom yang diinginkan
    printf("\tName\t\t\tAge\t\t\tClub\t\t\tNationality\t\tPotential\t\t\t\tPhoto\n");

    // Baca dan proses baris-baris selanjutnya
    while (fgets(line, sizeof(line), file)) {
        char* token = strtok(line, ",");
        char* name;
        char* age;
        char* club;
        char* nationality;
        char* potential;
        char* photo;

        // Cari indeks kolom yang diperlukan
        int index = 0;
        while (token != NULL) {
            if (index == 1) {
                name = token;
            } else if (index == 2) {
                age = token;
            } else if (index == 8) {
                club = token;
            } else if (index == 4) {
                nationality = token;
            } else if (index == 7) {
                potential = token;
            } else if (index == 3) {
                photo = token;
            }

            token = strtok(NULL, ",");
            index++;
        }

        // Cek kriteria umur dan klub
        if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
            // Cetak nilai yang diinginkan
            printf("%20s\t%10s\t\t%10s\t%25s\t%15s\t\t%60s\n", name, age, club, nationality, potential, photo);
        }
    }

    // Close file FIFA23_official_data.csv
    fclose(file);
}

int main(){
    pid_t pid = fork();
    int status;
    // Parent process
    if (pid < 0) {
        perror("Error: Fork failed");
        exit(1);
    } 
    // Child process
    else if (pid == 0){
        // Download file
        download();
        // Extract file
        unzip();
    }
    wait(&status);

    printf("\nBerikut ini merupakan data pemain FIFA23:\n");
    // Baca dan analisis file
    analyzeData();
    return 0;
}