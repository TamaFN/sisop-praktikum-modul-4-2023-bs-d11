#define FUSE_USE_VERSION 28
#include <stdlib.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/tama/Documents";

static int is_module_dir(const char *path) {
    // Check if the directory name starts with "module_"
    char *dir_name = strdup(basename(strdup(path)));
    if (strstr(dir_name, "module_") == dir_name) {
        free(dir_name);
        return 1;
    }
    free(dir_name);
    return 0;
}

static void log_system_call(const char *level, const char *command, const char *desc1, const char *desc2) {
    char log_file_path[PATH_MAX];
    strcpy(log_file_path, "/home/tama/fs_module.log");
    FILE *log_file = fopen(log_file_path, "a");

    if (log_file != NULL) {
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        fprintf(log_file, "[%s]::%02d%02d%02d-%02d:%02d:%02d::%s::%s", level,
                tm.tm_year % 100, tm.tm_mon + 1, tm.tm_mday,
                tm.tm_hour, tm.tm_min, tm.tm_sec,
                command, desc1);

        if (desc2 != NULL) {
            fprintf(log_file, "::%s", desc2);
        }

        fprintf(log_file, "\n");
        fclose(log_file);
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);
    
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        //if (filler(buf, de->d_name, &st, 0))
          //  break;
        
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }
    closedir(dp);

    // Log the system call
    log_system_call("REPORT", "READDIR", path, NULL);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    res = rename(from, to);
    if (res == -1)
        return -errno;

    // Check if the renamed directory is a module
    if (is_module_dir(from) && !is_module_dir(to)) {
        // Restore the original content by combining the small files into one
        DIR *dp;
        struct dirent *de;
        char small_file[PATH_MAX];

        dp = opendir(to);
        if (dp != NULL) {
            FILE *out_file = fopen(to, "wb");
            if (out_file != NULL) {
                while ((de = readdir(dp)) != NULL) {
                    if (strncmp(de->d_name, ".", 1) != 0 && strncmp(de->d_name, "..", 2) != 0) {
                        sprintf(small_file, "%s/%s", to, de->d_name);
                        FILE *in_file = fopen(small_file, "rb");
                        if (in_file != NULL) {
                            char buffer[1024];
                            size_t bytesRead;

                            while ((bytesRead = fread(buffer, 1, sizeof(buffer), in_file)) > 0) {
                                fwrite(buffer, 1, bytesRead, out_file);
                            }

                            fclose(in_file);
                            remove(small_file);
                        }
                    }
                }

                fclose(out_file);
            }

            closedir(dp);
        }

        // Rename the contents of subdirectories recursively
        dp = opendir(to);
        if (dp != NULL) {
            while ((de = readdir(dp)) != NULL) {
                if (de->d_type == DT_DIR) {
                    if (strncmp(de->d_name, ".", 1) != 0 && strncmp(de->d_name, "..", 2) != 0) {
                        char sub_from[PATH_MAX], sub_to[PATH_MAX];
                        sprintf(sub_from, "%s/%s", to, de->d_name);
                        sprintf(sub_to, "%s/%s", from, de->d_name);
                        xmp_rename(sub_from, sub_to);
                    }
                }
            }

            closedir(dp);
        }
    }

    // Log the system call
    if (is_module_dir(to)) {
        log_system_call("FLAG", "RENAME", from, to);
    } else {
        log_system_call("REPORT", "RENAME", from, to);
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    // Check if the created directory is a module
    if (is_module_dir(path)) {
        // Append " (modular)" to the directory name
        size_t path_len = strlen(path);
        char new_path[path_len + 12];
        strcpy(new_path, path);
        strcat(new_path, " (modular)");
        res = rename(path, new_path);
        if (res == -1)
            return -errno;

        // Log the system call
        log_system_call("FLAG", "MKDIR", path, NULL);
    } else {
        // Log the system call
        log_system_call("REPORT", "MKDIR", path, NULL);
    }

    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    int res;
    res = creat(path, mode);
    if (res == -1)
        return -errno;

    close(res);

    // Check if the created file is in a module directory
    char *dir_path = strdup(path);
    char *dir_name = dirname(dir_path);
    if (is_module_dir(dir_name)) {
        // Split the file into small files
        FILE *in_file = fopen(path, "rb");
        if (in_file != NULL) {
            char small_file[PATH_MAX];
            size_t chunkSize = 1024;
            size_t bytesRead;
            int count = 0;

            while ((bytesRead = fread(small_file, 1, chunkSize, in_file)) > 0) {
                sprintf(small_file, "%s.%03d", path, count++);
                FILE *out_file = fopen(small_file, "wb");
                if (out_file != NULL) {
                    fwrite(small_file, 1, bytesRead, out_file);
                    fclose(out_file);
                }
            }

            fclose(in_file);
            remove(path);
        }
    }

    free(dir_path);

    // Log the system call
    if (is_module_dir(dirname("/home/tama/fs_module.log"))) {
        log_system_call("FLAG", "CREATE", path, NULL);
    } else {
        log_system_call("REPORT", "CREATE", path, NULL);
    }

    return 0;
}

static int xmp_rmdir(const char *path)
{
    int res;
    res = rmdir(path);
    if (res == -1)
        return -errno;

    // Log the system call
    log_system_call("FLAG", "RMDIR", path, NULL);

    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    res = unlink(path);
    if (res == -1)
        return -errno;

    // Log the system call
    log_system_call("FLAG", "UNLINK", path, NULL);

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .mkdir = xmp_mkdir,
    .create = xmp_create,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

